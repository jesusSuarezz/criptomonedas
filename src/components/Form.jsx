import React, { useEffect, useState } from 'react';
import styled from '@emotion/styled';
import axios from 'axios';

import useMoneda from '../hooks/useMoneda';
import useCriptomoneda from '../hooks/useCriptomoneda';
import Mensaje from './Mensaje';

const Boton = styled.input`
	margin-top: 20px;
	font-weight: bold;
	font-size: 20px;
	padding: 10px;
	background-color: #66a2fe;
	border: none;
	width: 100%;
	border-radius: 10px;
	color: #fff;
	transition: background-color 0.3s ease;

	&:hover {
		background-color: #326ac0;
		cursor: pointer;
	}
`;

const Form = ({ guardarMoneda, guardarCriptomoneda }) => {
	//state del listado de criptomonedas
	const [listacripto, guardarCriptomonedas] = useState([]);
	const [error, guardarError] = useState(false);

	const MONEDAS = [
		{ codigo: 'USD', nombre: 'Dolar de Estado Unidos' },
		{ codigo: 'MXN', nombre: 'Peso Mexicano' },
		{ codigo: 'EUR', nombre: 'Euro' },
		{ codigo: 'GBP', nombre: 'Libra Esterlina' },
	];

	//Utilizando nuestro hook
	const [moneda, SelectMonedas] = useMoneda('Elige tu moneda', '', MONEDAS);
	//utilizamos nuestro hook criptomoneda
	const [criptomoneda, SelectCripto] = useCriptomoneda(
		'Elige tu criptomoneda',
		'',
		listacripto
	);

	useEffect(() => {
		const consultarAPI = async () => {
			const url =
				'https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=USD';
			const resultado = await axios.get(url);
			console.log(resultado.data.Data);

			guardarCriptomonedas(resultado.data.Data);
		};
		consultarAPI();
	}, []);

	//cuando el usuario hace submit
	const onSubmit = (e) => {
		e.preventDefault();

		//validar si ambos selects estan llenos
		if (moneda === '' || criptomoneda === '') {
			guardarError(true);
			return;
		}

		//pasar los datos al componentes principal
		guardarError(false);
		guardarMoneda(moneda);
		guardarCriptomoneda(criptomoneda);
	};

	return (
		<form onSubmit={onSubmit}>
			{error ? <Mensaje mensaje="Todos los campos son obligatorios" /> : null}
			<SelectMonedas />
			<SelectCripto />
			<Boton type="submit" value="Calcular" />
		</form>
	);
};

export default Form;

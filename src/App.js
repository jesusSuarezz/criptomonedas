import React, { useState, useEffect } from 'react';

import styled from '@emotion/styled';
import axios from 'axios';

import imagen from './cryptomonedas.png';
import Form from './components/Form';
import Cotizacion from './components/Cotizacion';
import Spinner from './components/Spinner';

const Contenedor = styled.div`
	max-width: 900px;
	margin: 0 auto;
	@media (min-width: 992px) {
		display: grid;
		grid-template-columns: repeat(2, 1fr);
		column-gap: 2rem;
	}
`;

const Imagen = styled.img`
	max-width: 100%;
	margin-top: 5rem;
`;
const Heading = styled.h1`
	font-family: 'Bebas Neue', cursive;
	color: #fff;
	text-align: left;
	font-weight: 700;
	font-size: 50px;
	margin-bottom: 50px;
	margin-top: 80px;
	&::after {
		content: '';
		width: 100px;
		height: 6px;
		background-color: #66a2fe;
		display: block;
	}
`;

function App() {
	const [moneda, guardarMoneda] = useState('');
	const [criptomoneda, guardarCriptomoneda] = useState('');

	const [resultado, guardarResultado] = useState({});
	const [cargando, guardarCargando] = useState(false);

	useEffect(() => {
		const cotizarCriptomoneda = async () => {
			//Evitamos la ejecutcion del calculo la primera vez
			if (moneda === '') return;
			console.log('cotizando...');

			//consulta de la papi para obtener la cotizacion
			const url = `https://min-api.cryptocompare.com/data/pricemultifull?fsyms=${criptomoneda}&tsyms=${moneda}`;
			const response = await axios.get(url);

			//mostrando la animacion preloading
			guardarCargando(true);
			//ocultar el spinner y mostrar el resultado de la API
			setTimeout(() => {
				//quitamos la animacion
				guardarCargando(false);

				//Guardar la cotizacion
				guardarResultado(response.data.DISPLAY[criptomoneda][moneda]);
			}, 2000);

			console.log(resultado);
		};

		cotizarCriptomoneda();
	}, [moneda, criptomoneda]);

	//Mostrar spinner o resultado
	const componente =  (cargando) ? <Spinner /> : <Cotizacion resultado={resultado} />

	return (
		<Contenedor>
			<div>
				<Imagen src={imagen} alt="imagen criptomoneda" />
			</div>
			<div>
				<Heading>Cotiza criptomonedas al instante (Jesús Suárez)</Heading>
				<Form
					guardarMoneda={guardarMoneda}
					guardarCriptomoneda={guardarCriptomoneda}
				/>

				{componente}
			</div>
		</Contenedor>
	);
}

export default App;
